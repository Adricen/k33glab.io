const fs = require('fs')
const yamlFront = require('yaml-front-matter')
const path = require('path')

const docFolder = './docs/articles/'
const homepage = 'https://k33g.gitlab.io'

let feed = {
  version: 'https://jsonfeed.org/version/1',
  user_comment: 'Json Feed from k33g.gitlab.io',
  title: 'k33g.gitlab.io - JSON Feed',
  description: 'Geek Blog Posts by @k33g_org',
  home_page_url: homepage,
  feed_url: `${homepage}/feed.json`,
  author: {
    name: '@k33G_org',
    url: 'https://twitter.com/k33g_org?lang=en'
  },
  items: []
}

fs.readdirSync(docFolder).forEach(file => {

  if(path.parse(file).name !== ".DS_Store" && path.parse(file).name !== "images") {
    console.log(`${homepage}/articles/${path.parse(file).name}.html`)

    let contents = fs.readFileSync(`${docFolder}${file}`, 'utf8')
    let yaml = yamlFront.loadFront(contents)
  
    feed.items.push({
      title: yaml.title,
      date_published: yaml.date,
      id: `${homepage}/articles/${path.parse(file).name}.html`,
      url: `${homepage}/articles/${path.parse(file).name}.html`,
      content_html: yaml.classification + (yaml.teaser!=="" ? ` | ${yaml.teaser}` : "") // __content:
    })
  }

})
feed.items.reverse()
fs.writeFileSync('./public/feed.json', JSON.stringify(feed,null,2));

/*
npm install yaml-front-matter -save
*/
