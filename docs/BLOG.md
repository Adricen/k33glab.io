# Blog Posts

> WIP :construction:

## Articles from this Blog

<all-articles/>

## Articles from Medium

- 🇬🇧 [Quick and easy VuePress website thanks to GitLab Pages](https://medium.com/@k33g_org/quick-and-easy-vuepress-website-thanks-to-gitlab-pages-6e02e631d1e)
- 🇬🇧 [Deploy a GitLab runner on Clever Cloud](https://medium.com/@k33g_org/deploy-a-gitlab-runner-on-clever-cloud-367a3098e90a)
- 🇬🇧 [Create and deploy a Nuxt.js application on Clever-Cloud in a few moments](https://medium.com/@k33g_org/create-and-deploy-a-nuxt-js-application-on-clever-cloud-in-a-few-moments-47474059d849)
- 🇫🇷 [1er contact avec RedPipe](https://medium.com/@k33g_org/1er-contact-avec-redpipe-c80ef176e44f)
- 🇫🇷 [Faire une Review App avec CleverCloud et GitLab CI](https://medium.com/@k33g_org/faire-une-review-app-avec-clevercloud-et-gitlab-ci-ce56d8d4538b)
- 🇫🇷 [Créer une chaîne de publication d’e-book(s) avec GitLab, GitLab-CI, Vagrant & AsciiDoctor](https://medium.com/@k33g_org/cr%C3%A9er-une-cha%C3%AEne-de-publication-de-book-s-avec-gitlab-gitlab-ci-vagrant-asciidoctor-46e6f7ef7e71)
- 🇫🇷 [Déployer un runner GitLab sur Clever Cloud](https://medium.com/@k33g_org/d%C3%A9ployer-un-runner-gitlab-sur-clever-cloud-2a804d00d1ab)

## Older

- See my old blog on GitHub Pages: [http://k33g.github.io/](http://k33g.github.io/)