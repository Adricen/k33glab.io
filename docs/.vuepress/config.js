module.exports = {
  title: "K33G's website",
  description: "🇫🇷 and 🇬🇧 blog posts",
  dest: "public",
  ga: "UA-123883795-1",
  body: [],
  themeConfig: {
    lastUpdated: 'Last Updated', // string | boolean
    sidebar: [
        '/'
      , '/BLOG' 
      , '/RESOURCES'
      , '/ABOUT'
    ]
  },
  plugins: []
}