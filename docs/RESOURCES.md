# Resources

> WIP :construction:


## JavaScript

### Vue

#### VuePress

- [Adding a Recent Content Component to VuePress](https://www.raymondcamden.com/2018/05/09/adding-a-recent-content-component-to-vuepress)

#### Functional programming

- [Type checking and DDD for JavaScript](https://github.com/gcanti/tcomb)

<disqus/>