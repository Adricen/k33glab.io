---
home: true
actionText: 👋 Enter 🏠
actionLink: /BLOG
features:
- title: Programming
  details: I ❤️ JavaScript, Scala, Golo and Kotlin
- title: CI/CD
  details: I 💚 DevOps, PaaS, GitLab Runners, ...
- title: Tools, methods, ...
  details: I'm a geek, and I 💙 a lot of thinks like IOT, fishing, cooking, ...
footer: MIT Licensed | Copyright © 2018-2019 Philippe Charrière
---

<!--
<confoo-votes/>
-->

<follow-me/>

<last-articles/>

<jsonfeed-link/>


